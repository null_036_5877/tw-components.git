// import { fileURLToPath, URL } from 'node:url'
//
// import { defineConfig } from 'vite'
// import vue from '@vitejs/plugin-vue'
// import vueJsx from '@vitejs/plugin-vue-jsx'
//
// // https://vitejs.dev/config/
// export default defineConfig({
//   plugins: [
//     vue(),
//     vueJsx(),
//   ],
//   resolve: {
//     alias: {
//       '@': fileURLToPath(new URL('./src', import.meta.url))
//     }
//   }
// })
// @ts-ignore
import {defineConfig} from 'vite'
// @ts-ignore
import {devConfig} from './vite.config.dev.ts'
// @ts-ignore
import {proConfig} from './vite.config.pro.ts'

// @ts-ignore
export default defineConfig(({ command, mode }) => {
  if (command === 'serve') {
    // dev 独有配置
    return devConfig(devConfig)
  } else {
    // command === 'build'
    return proConfig(mode)
  }
})
