// @ts-ignore
import app from '../main.ts'
import { ElMessageBox } from 'element-plus'
import dayjs from 'dayjs'
import isSameOrAfter from 'dayjs/plugin/isSameOrAfter'
import isSameOrBefore from 'dayjs/plugin/isSameOrBefore'
// @ts-ignore
import lodash from 'lodash'
import {nextTick} from '@vue/runtime-core'
import { setupGlobDirectives } from '@/directive/loadMore.js'

nextTick(() => {
    /**
     * 非空判断
     * */
    app.config.globalProperties.notNull = function (flag) {
        return flag !== null && flag !== '' && flag !== 'null' && flag !== undefined
    }
    /**
     * 手机号脱敏处理
     * */
    app.config.globalProperties.phoneAllergic = function (tel) {
        let reg = /^(\d{3})\d{4}(\d{4})$/
        return tel ? tel.replace(reg, '$1****$2') : ''
    }
    /**
     * 保留digits 位小数
     * */
    app.config.globalProperties.toDecimal = function (num: any, digits: any) {
        let dig = 2
        if (digits) {
            dig = digits
        }
        var f = parseFloat(num);
        if (isNaN(f)) {
            return num;
        }
        var f = Math.round(num * Math.pow(10, dig)) / Math.pow(10, dig);
        var s = f.toString();
        var rs = s.indexOf(".");
        if (rs < 0) {
            rs = s.length;
            s += ".";
        }
        while (s.length <= rs + digits) {
            s += "0";
        }
        return s;
    };


    app.config.globalProperties.MessageBox = function (data) {
        if (!data.title) {
            data.title = '提示'
        }
        if (!data.confirmText) {
            data.confirmText = '确定'
        }
        if (!data.cancelText) {
            data.cancelText = '取消'
        }
        if (!data.type) {
            data.type = ''
        }

        // if(!data.center){ data.center=false}

        return new Promise(function (resolve, reject) {
            ElMessageBox.confirm(data.msg, data.title, {
                type: data.type,
                confirmButtonText: data.confirmText,
                cancelButtonText: data.cancelText,
                customClass: 'tw_msgBox',
                center: true,
                dangerouslyUseHTMLString: true,
                distinguishCancelAndClose: true,
                closeOnClickModal: false,
                beforeClose: (action, instance, done) => {
                    if (action === 'confirm') {
                        instance.confirmButtonLoading = true
                        instance.confirmButtonText = '执行中...'

                        function close() {
                            done()
                            instance.confirmButtonLoading = false
                        }

                        // @ts-ignore
                        resolve(close, instance)
                    } else {
                        done()
                        reject(action)
                    }
                },
            }).then((action) => {
                reject(action)
            })
        })
    }


    /**
     * 下载文件
     * */
    app.config.globalProperties.download = function (res, name: '文件', type: 'xls') {
        let detailType = {
            xlsx: 'vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            xls: 'vnd.ms-excel',
            pdf: 'pdf',
            zip: 'zip',
            doc: 'msword',
            docx: 'vnd.openxmlformats-officedocument.wordprocessingml.document',
        }[type || 'xlsx']
        let blobData = new Blob([res], {type: `application/${detailType};charset=utf-8`})
        const url = window.URL.createObjectURL(blobData)
        let a = document.createElement('a')
        // @ts-ignore
        a['style'] = 'display: none'
        a.href = url
        a.download = name
        // a.setAttribute('download', decodeURI(Date.now()+name));
        document.body.appendChild(a)
        a.click()
        document.body.removeChild(a)
        window.URL.revokeObjectURL(url)
    }
    setupGlobDirectives(app)
    // getSelectVal(app)
    dayjs.extend(isSameOrAfter)
    dayjs.extend(isSameOrBefore)
    app.config.globalProperties.$dayjs = dayjs
    app.config.globalProperties.$_ = lodash
    app.config.globalProperties.$oss = '?x-oss-process=image/resize,w_100,limit_0'
})
