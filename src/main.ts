import './assets/main.css'

import { createApp } from 'vue'

import App from './App.vue'
import router from './router'
// @ts-ignore
import ElementPlus, {ElMessage} from 'element-plus';
import 'element-plus/dist/index.css'
// errorHandler
import errorHandler from "@/utils/errorHandler";

// @ts-ignore
import zhCn from 'element-plus/es/locale/lang/zh-cn'
import './styles/index.scss'
import './utils/globalProperties.ts'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'



const app = createApp(App)


// 默认自用引入ElementPlus的所有icon
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}

/**
 * 调整提示配置
 * */
let arr = ['success', 'warning', 'info', 'error']
arr.forEach(type => {
    // @ts-ignore
    ElMessage[type] = options => {
        if (typeof options === 'string') {
            options = {
                //options参数只有message一个
                message: options, showClose: true, offset: window.innerHeight/2-23
            };
        }
        options.type = type;
        return ElMessage(options);
    };
});

// app.config.errorHandler = errorHandler;
app.use(router)
app.use(ElementPlus,{locale: zhCn})

app.mount('#app')

export default app
