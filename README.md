# tw-vue-el

这个组件是基于在Vite中使用Vue 3和ElmentPlus（二次封装）进行开发，可以帮助您快速开发form和列表页面。默认引入了ElmentPlus基本多少有的组件。可以直接使用


## 自定义配置

请参见 [Vite配置参考](https://vitejs.dev/config/)。

## 项目使用

```sh
npm install tw-vue-el

mian.ts
import twVueEl from 'tw-vue-el'
import 'tw-vue-el/dist/style.css'
```

### 完整引入

```sh
import { createApp } from "vue"
import App from './App.vue'
import router from "./router/index"
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import zhCn from 'element-plus/es/locale/lang/zh-cn'
import twVueEl from 'tw-vue-el'
// 因为表格组件和form组件需要引入相关css
import 'tw-vue-el/dist/style.css'

const app = createApp(App)

app.use(ElementPlus,{locale: zhCn})
app.use(router)
app.use(twVueEl)
app.use(store)
app.mount('#app')

```

### 相关组件

```sh
<ContentMain></ContentMain>                             // --- 表格搜索
<ContentMainVirtualized></ContentMainVirtualized>       // --- 表格虚拟列表搜索
<CountTo></CountTo>                                     // --- 计步器
<TwForm></TwForm>                                       // --- form组件
<TwCustomForm></TwCustomForm>                           // --- 自定义form组件
<TwDialog></TwDialog>                                   // --- 弹框组件
<TwDrawer></TwDrawer>                                   // --- 抽屉组件
<TwFormItem></TwFormItem>                               // --- 自定义显示控件（基于formItem） 
<TwPagination></TwPagination>                           // --- 分页组件
<TwTable></TwTable>                                     // --- 表格组件
<TwVirtualizedTable></TwVirtualizedTable>               // --- 虚拟表格组件
```

### 表格搜索（ContentMain）
>ContentMain 是tw-form和tw-table的集合它只接收一个参数mainData,内部的参数才是使用规则.此处只有基础配置属性，详情请跳转相关组件属性查看

##### ContentMain 使用示例
```
<template>
    <content-main ref="contentMainRef" :mainData="mainData"></content-main>
</template>

<script setup lang="tsx">
    let contentMainRef = $ref(null)
    
    let mainData = reactive({
        tableUrl: '/operation/appealHand/page',
        isCheck: true,
        columns: [
            {prop: 'appealTimeStr', label: '反馈时间', width: 180},
            {prop: 'alarmTypeName', label: '报警类型', width: 120},
            {prop: 'durationStr', label: '持续时长', width: 120},
            {prop: 'appealStatusName', label: '处理状态', width: 120},
            {prop: 'appealResultName', label: '处理结果', width: 120},
            {prop: 'processPerson', label: '处理人', width: 120},
            {prop: 'processTimeStr', label: '处理时间', width: 120},
        ],
        headBtns: [],
        tableBtns: {
            prop: 'btns',
            label: '操作',
            fixed: 'right',
            width: 100,
            btns: [
            ],
        },
        formData: {
            alarmLevel: 0,
            pageNumber: 1,
            pageSize: 50,
        },
        lists: [],
    })
</script> 
```


#### ContentMain 属性

| 属性名                 | 说明                                                                                         | 类型       | 默认值                                     |
|---------------------|--------------------------------------------------------------------------------------------|----------|-----------------------------------------|
| lists               | 用于渲染表格上面搜索                                                                                 | Array    | []                                      |
| foldNumber          | 第一行默认显示数量                                                                                  | Number   | 3                                       |
| rules               | 验证规则（同[el-form](https://element-plus.gitee.io/zh-CN/component/form.html)）                  | Object   | {}                                      |
| formData            | 同lists的key绑定使用，同时也会默认和表格api绑定获取数据                                                          | Object   | {}                                      |
| labelWidth          | 搜索区域文字描述宽度                                                                                 | String   | '120px'                                 |
| isFormBnts          | 是否显示tw-form相关的操作按钮                                                                         | Boolean  | true                                    |
| beforeChange        | 点击搜索按钮时间前触发的事件                                                                             | Function | -                                       |
| url                 | 表格请求地址                                                                                     | String   | -                                       |
| id                  | table的唯一id，一页面多表格场景须赋值，否则无法动态计算高度                                                          | String   | -                                       |
| method              | 表格动态url请求类型                                                                                | String   | post                                    |
| columns             | 表格每一列渲染数据                                                                                  | Array    | []                                      |
| lists               | 表格每一行渲染数据                                                                                  | Array    | []                                      |
| tableParams         | 表格默认请求数据，这里代指mainData.formData。他们是绑定关系                                                     | Object   | { pageNumber: 1, pageSize: 25 }         |
| tableHeight         | 表格固定高度                                                                                     | Number   | 0                                       |
| isRradio            | 单选                                                                                         | Boolean  | false                                   |
| showPagination      | 分页显示开关                                                                                     | Boolean  | true                                    |
| botHeight           | 是分页栏加上边距加底部空白位置高度                                                                          | Number   | 50                                      |
| indexFixed          | 索引左侧定位开关                                                                                   | Boolean  | false                                   |
| isCheck             | checkbox显示开关                                                                               | Boolean  | false                                   |
| checkFixed          | checkbox左侧定位开关                                                                             | String   | 'left'                                  |
| showIndex           | 索引显示开关                                                                                     | Boolean  | false                                   |
| size                | 表格大小配置                                                                                     | String   | 'large'                                 |
| stripe              | 斑马纹显示开关                                                                                    | Boolean  | true                                    |
| border              | 边框线显示开关                                                                                    | Boolean  | false                                   |
| pageSizesArr        | 分页页码                                                                                       | Array    | [25, 50, 100]                           |
| tableBtns           | 单例按钮渲染数据                                                                                   | Object   | -                                       |
| small               | 每页显示条数                                                                                     | Boolean  | false                                   |
| headBtns            | 顶部操作按钮                                                                                     | Array    | []                                      |
| isTableHeader       | 默认显示表格顶部操作栏                                                                                | Boolean  | true                                    |
| isLoadStatus        | 默认首次不加载table数据                                                                             | Boolean  | false                                   |
| highlightCurrentRow | 是否高亮显示当前选中行                                                                                | Boolean  | true                                    |
| rowClassName        | 行样式名称，同[elTable](https://element-plus.gitee.io/zh-CN/component/table.html)一样               | Function | -                                       |
| selectable          | isCheck必须为true 用于判断选项是否可以点击，返回true或false                                                   | Function | { return true}                          |
| autoScrollToTop     | 请求数据后是否自动滚动到顶部                                                                             | Boolean  | true                                    |
| layout              | 表格分页布局默认配置，依赖[elPagination](https://element-plus.gitee.io/zh-CN/component/pagination.html) | String   | total, prev, pager, next, sizes, jumper |
| background          | 表格的背景颜色                                                                                    | Boolean  | true                                    |
| loadingBg           | 表格接口加载状态下背景颜色                                                                              | String   | rabg(225,225,225)                       |
| emptyText           | 列表无数据时展示的文字                                                                                | String   | 暂无数据                                    |

#### ContentMain 事件
| 事件名                | 说明                                                                                                     | 类型       |
|--------------------|--------------------------------------------------------------------------------------------------------|----------|
| tableCallback      | 表格api加载完成后响应的事件                                                                                        | Function |
| checkSelect        | 表格点击checkbox响应的事件，对应[elTable](https://element-plus.gitee.io/zh-CN/component/table.html) - select事件     | Function |
| rowCheck           | 表格点击行数据响应的事件                                                                                           | Function |
| tableCurrentChange | 当表格的当前行发生变化的时候会触发该事件，如果要高亮当前行，请打开表格的 highlight-current-row 属性                                          | Function |
| resetForm          | form点击重置按钮响应的事件                                                                                        | Function |
| submitForm         | form点击搜索按钮响应的事件                                                                                        | Function |
| selectionChange    | 表格的check选择， 对应[elTable](https://element-plus.gitee.io/zh-CN/component/table.html) - selection-change事件 | Function |

#### ContentMain Exposes
| 事件名                | 说明             | 类型       |
|--------------------|----------------|----------|
| getTable           | 主动触发表格加载       | Function |
| seekTable          | 模拟触发form搜索事件功能 | Function |
| getAllCheck        | 获取表格已勾选数据      | Function |
| getTableLists      | 获取表格已渲染行数据     | Function |
| setTableLists      | 设置表格待渲染的行数据    | Function |
| setCurrentRow      | 设置表格行高亮        | Function |
| clearSelection     | 清空表格已勾选数据      | Function |
| toggleRowSelection | 设置表格行数据选择      | Function |
| setTableTotal      | 设置表格Total数值    | Function |
| setTableDataHeight | 重新计算表格的自适应高度   | Function |

#### ContentMain 插槽
>tw-form  lists单个对象自定义key就是自定义插槽，适场景使用。虽然提供此功能，但不建议使用

| 事件名            | 说明              |
|----------------|-----------------|
| tableHeader    | 表格按钮上方自定义区域     |
| table-l        | 同表格按钮一行，左侧可操作区域 |
| table-c        | 同表格按钮一行，中间可操作区域 |
| table-r        | 同表格按钮一行，右侧可操作区域 |
| tableBotHeader | 表格按钮下方自定义区域     |


### 表格虚拟列表搜索（ContentMainVirtualized）
>ContentMainVirtualized 如需使用查看如下描述后在使用
1. 是tw-form和TwVirtualizedTable的集合它只接收一个参数mainData,
2. 本质如同contentMain,只是加载行为是滚动加载更多数据并且处理了keep-alive切换页面后的部分兼容问题
3. 其次因为大批量数据一般不会每次提供总数，所以数据接口和总数接口是做了分离的

##### ContentMainVirtualized 使用示例
```
<template>
    <content-main-virtualized ref="contentMainRef" :mainData="mainData"></content-main-virtualized>
</template>

<script setup lang="tsx">
    let contentMainRef = $ref(null)
    
    let mainData = reactive({
        tableUrl: '/operation/appealHand/page',
        totalUrl: '/operation/appealHand/pageCount',
        isCheck: true,
        selectable: (row, index) => {
            return row.appealStatusCode !== 300
        },
        columns: [
            {prop: 'appealTimeStr', label: '反馈时间', width: 180},
            {prop: 'alarmTypeName', label: '报警类型', width: 120},
            {prop: 'durationStr', label: '持续时长', width: 120},
            {prop: 'appealStatusName', label: '处理状态', width: 120},
            {prop: 'appealResultName', label: '处理结果', width: 120},
            {prop: 'processPerson', label: '处理人', width: 120},
            {prop: 'processTimeStr', label: '处理时间', width: 120},
        ],
        headBtns: [],
        tableBtns: {
            prop: 'btns',
            label: '操作',
            fixed: 'right',
            width: 100,
            btns: [
            ],
        },
        formData: {
            alarmLevel: 0,
            pageNumber: 1,
            pageSize: 50,
        },
        lists: [],
    })
</script> 
```

### 统计（CountTo）
> 显示统计数据。


#### CountTo 属性
| 属性名       | 说明         | 类型       | 默认值  |
|-----------|------------|----------|------|
| startVal  | 动画开始值      | Number   | 0    |
| endVal    | 动画结束值      | Number   | 0    |
| duration  | 动画滚动时间     | Number   | 3000 |
| autoplay  | 是否自动播放     | Boolean  | true |
| decimals  | 保留几位小数     | Number   | 0    |
| decimal   | 小数         | String   | .    |
| separator | 千分位符号      | String   | ,    |
| prefix    | 统计值前置自定义文本 | String   | -    |
| suffix    | 统计值后置自定义文本 | String   | -    |
| useEasing | 使用缓和       | Boolean  | true |
| easingFn  | 缓和回调       | Function | -    |


#### CountTo 方法
| 方法名             | 说明       | 类型       |
|-----------------|----------|----------|
| mountedCallback | 挂载后返回的回调 | Function |
| callback        | 动画后返回的回调 | Function |




### form组件（TwForm）
> 此组件只服务于contentMain,核心用法请综合TwCustomForm和contentMain描述区域使用。
> 注意：请不要混淆TwForm和TwCustomForm组件。 TwCustomForm会更轻量，便于更复杂的form场景使用

### 自定义form组件（TwCustomForm）

> 注意：请不要混淆TwForm和TwCustomForm组件。 TwCustomForm会更轻量，便于更复杂的form场景使用。请合理使用TwCustomForm组件

TwCustomForm组件自带placeholder，同时placeholder是跟label强关联的。并且可以自定义使用slot</br>
1. 输入框提示：请输入年龄
2. 选择框提示：请选择年龄
3. 自定义使用slot需先声明slot为true，然后自定义编写具名template
4. 公共属性配置有type（定义需要显示的控件）和col（默认24，24等分同el-row的el-col）
##### 基础使用示例
```
// 虽然vue3的模板语言说可以不在用div包着了，但是如果不包很有很多隐性的bug。请不要节省这个时间
// 虽然vue3的模板语言说可以不在用div包着了，但是如果不包很有很多隐性的bug。请不要节省这个时间
// 虽然vue3的模板语言说可以不在用div包着了，但是如果不包很有很多隐性的bug。请不要节省这个时间

<template>
    <div>
        <tw-custom-form
                ref="fromDataRef"
                :lists="mainData.lists"
                :rules="mainData.rules"
                :formData="mainData.formData"
            >
               <template #securityProtocol>
                  这里是持续时长的自定义
               </template>
            </tw-custom-form>
        <el-button @click="save">保存</el-button>
    </div>
     
</template>

<script setup lang="tsx">
    let fromDataRef = $ref(null)
    let mainData = reactive({
        lists: [
            {key: 'entryPointName', label: '反馈时间'},
            {key: 'basicProtocol', label: '报警类型'},
            {key: 'securityProtocol', label: '持续时长', slot: true},  // 自定义slot
        ],
        rules: {
            entryPointName: [
                { required: true, message: '请输入配置名称', trigger: 'blur'},
            ],
            basicProtocol: [
                { required: true, message: '请选择协议版本', trigger: 'blur'},
            ],
            securityProtocol: [
                { required: true, message: '请选择主动安全', trigger: 'blur'},
            ],
        },
    })
    
    // 组件内置有svae方法，详情请查看配置。
    const save = () => {
        fromDataRef.submitForm(false).then((form) => {
            loading = true
            addOrUpdate(form).then(() => {
                loading = false
            }).catch(() => {
                loading = false
            })
        })
    }
</script> 
```
> 自定义form组件最关键的就是lists参数，lists又是根据type来转义到相关内部组件，以下是能使用的type属性。type属性默认就是twInput（输入框）如上示例


#### TwCustomForm 属性
| 属性名          | 说明                                                                        | 类型       | 默认值     |
|--------------|---------------------------------------------------------------------------|----------|---------|
| lists        | 用于渲染表格上面搜索。                                                               | Array    | []      |
| rules        | 验证规则（同[el-form](https://element-plus.gitee.io/zh-CN/component/form.html)） | Object   | {}      |
| formData     | 同lists的key绑定使用，同时也会默认和表格api绑定获取数据                                         | Object   | {}      |
| labelWidth   | 搜索区域文字描述宽度                                                                | String   | '120px' |
| isFormBnts   | 是否显示tw-form相关的操作按钮                                                        | Boolean  | false   |
| beforeChange | 点击保存按钮时间前触发的                                                              | Function | -       |

#### TwCustomForm 属性 Lists type

***label 和 key 是必传字段。其次type除了input框，也都是必传字段（具体请看下面配置）***

##### 1. twInput 输入框类型 </br>
```
{key: 'securityProtocol', label: '持续时长'}
```
col（默认24，24等分同el-row的el-col

| 属性名           | 说明                                                                                | 类型                 | 默认值     |
|---------------|-----------------------------------------------------------------------------------|--------------------|---------|
| col           | 响应式宽度设置                                                                           | Number             | 24      |
| labelWidth    | 标签单个宽度                                                                            | String             | '120px' |
| maxlength     | 同原生 maxlength 属性                                                                  | String/Number      | -       |
| minlength     | 原生属性，最小输入长度                                                                       | String/Number      | -       |
| max           | 原生 max 属性，设置最大值                                                                   | -                  | -       |
| min           | 原生属性，设置最小值                                                                        | -                  | -       |
| showWordLimit | 是否显示统计字数, 只在 type 为 'text' 或 'textarea' 的时候生效                                     | Boolean            | Boolean |
| placeholder   | 输入框占位文本                                                                           | String             | -       |
| showPassword  | 是否显示切换密码图标                                                                        | Boolean            | false   |
| disabled      | 是否禁用                                                                              | Boolean            | false   |
| size          | 输入框尺寸，只在 type 不为 'textarea' 时有效                                                   | Enum               | -       |
| prefixIcon    | 自定义前缀图标(el引入的icon)                                                                | Component          | -       |
| suffixIcon    | 自定义后缀图标(el引入的icon)                                                                | Component          | -       |
| rows          | 输入框行数，仅 type 为 'textarea' 时有效                                                     | Number             | Number  |
| minRows       | textarea 高度是否自适应，仅 type 为 'textarea' 时生效。 可以接受一个对象，比如: { minRows: 2, maxRows: 6 } | Number             | -       |
| maxRows       | textarea 高度是否自适应，仅 type 为 'textarea' 时生效。 可以接受一个对象，比如: { minRows: 2, maxRows: 6 } | Number             | -       |
| readonly      | 原生 readonly 属性，是否只读                                                               | Boolean            | false   |
| resize        | 控制是否能被用户缩放                                                                        | Enum               | -       |
| validateEvent | 输入时是否触发表单的校验                                                                      | Boolean            | true    |
| inputStyle    | input 元素或 textarea 元素的 style                                                      | string/object      | {}      |
| clearable     | 是否显示清除按钮，只有当 type 不是 textarea时生效                                                  | Boolean            | -       |
| props.type    | 类型                                                                                | string等原生 input 类型 | text    |

##### 2. select 选择框类型 </br>
```
// 基础
{key: 'securityProtocol', label: '持续时长', type: 'select', children: [ {label: '北京', value: 1'}]}

// 自定义label value
{key: 'securityProtocol', label: '持续时长', type: 'select', children: [ {labelZdy: '北京', valueZdy: 1'}], props: {label: 'labelZdy', value: 'valueZdy'}}

// 接口请求
{label: '车牌号', key: 'vehicleCode', type: 'select', remote: true, url: '/business/vehicle/associationPlateNum', method: 'post', loadMore: true, params: { keyword: route.query.plateNum ||'', pageNumber: 1, pageSize: 25, havePermission: true }, props: { label: 'plateNum', value: 'vehicleCode' }, placeholder: '请选择车牌号码',},

```
配置维度是依赖[el-select](https://element-plus.gitee.io/zh-CN/component/select.html)

| 属性名                | 说明                                                      | 类型       | 默认值     |
|--------------------|---------------------------------------------------------|----------|---------|
| col           | 响应式宽度设置                                                                           | Number             | 24      |
| url                | 接口地址，只有在remote: true情况使用                                | String   | -       |
| method             | 接口请求类型                                                  | String   | 'post'  |
| params             | 接口请求参数                                                  | Object   | {}      |
| labelWidth         | 标签单个宽度                                                  | String   | '120px' |
| multiple           | 是否多选                                                    | Boolean  | -       |
| collapseTags       | 多选时是否将选中值按文字的形式展示,不等于false就默认为true                      | Boolean  | -       |
| clearable          | 是否可以清空选项                                                | Boolean  | false   |
| disabled           | 是否禁用                                                    | Boolean  | false   |
| remote             | 其中的选项是否从服务器远程加载                                         | Boolean  | false   |
| defaultFirstOption | 是否在输入框按下回车时，选择第一个匹配项。 需配合 filterable 或 remote 使用        | Boolean  | false   |
| filterable         | Select 组件是否可筛选                                          | Boolean  | false   |
| allowCreate        | 是否允许用户创建新条目， 只有当 filterable 设置为 true 时才会生效              | Boolean  | false   |
| multipleLimit      | multiple 属性设置为 true 时，代表多选场景下用户最多可以选择的项目数， 为 0 则不限制     | Number   | 0       |
| reserveKeyword     | 当 multiple 和 filterable被设置为 true 时，是否在选中一个选项后保留当前的搜索关键词 | Boolean  | 	true   |
| children           | 自定义下拉渲染数据                                               | Array    | 	[]     |
| change             | 	选中值发生变化时触发                                             | Function | 	-      |

##### 3. date 日期选择器 </br>

```
// 基础
{label: '活动时间', key: 'date1', type:'date', showPassword: true },


// 日期时间选择
{label: '日期时间', key: 'date2', type:'date', data: {type: 'datetime'} },

// 时间格式
{ key: 'issueDate', label: '初次领证日期',col:12, type:'date', placeholder: '请输入初次领证日期', data: {valueFormat: 'YYYY-MM-DD'} },

```
配置维度是依赖[el-date-picker](https://element-plus.gitee.io/zh-CN/component/date-picker.html#%E5%B1%9E%E6%80%A7)

| 属性名          | 说明                                                   | 类型       | 默认值                 |
|--------------|------------------------------------------------------|----------|---------------------|
| col           | 响应式宽度设置                                                                           | Number             | 24      |
| data.type    | 显示类型                                                 | String   | date                |
| data.format  | 显示在输入框中的格式                                           | String   | YYYY-MM-DD HH:mm:ss |
| disabled     | 禁用                                                   | Boolean  | false               |
| placeholder  | 非范围选择时的占位内容                                          | String   | -                   |
| clearable    | 是否显示清除按钮                                             | Boolean  | false               |
| disabledDate | 一个用来判断该日期是否被禁用的函数，接受一个 Date 对象作为参数。 应该返回一个 Boolean 值 | Function | -                   |
| change       | 	选中值发生变化时触发                                          | Function | 	-                  |

##### 4. time 时间选择器 </br>


```
// 基础
{label: '开始时间', key: 'startTime', type: 'time'}

// 时间选择
{label: '开始时间', key: 'startTime', type: 'time', data: {valueFormat: 'HH:mm:ss'}}
```
配置维度是依赖[el-time-picker](https://element-plus.gitee.io/zh-CN/component/time-picker.html)

| 属性名             | 说明               | 类型          | 默认值                 |
|-----------------|------------------|-------------|---------------------|
| col           | 响应式宽度设置                                                                           | Number             | 24      |
| defaultValue    | 可选，选择器打开时默认显示的时间 | Date/object | -                   |
| data.format     | 显示在输入框中的格式       | String      | YYYY-MM-DD HH:mm:ss |
| disabled        | 禁用               | Boolean     | false               |
| placeholder     | 非范围选择时的占位内容      | String      | -                   |
| clearable       | 是否显示清除按钮         | Boolean     | false               |
| disabledHours   | 禁止选择部分小时选项       | Function    | -                   |
| disabledMinutes | 禁止选择部分分钟选项       | Function    | -                   |
| disabledSeconds | 禁止选择部分秒选项        | Function    | -                   |
| change          | 	选中值发生变化时触发      | Function    | 	-                  |



##### 5. switch 开关

```
// 基础
{label: '状态', key: 'state', col: 6, type: 'switch'},


// 自定义名称
{label: '状态', key: 'state', col: 6, type: 'switch', activeText: '启用', inactiveText: '禁用', filter: 'valid'},

```
配置维度是依赖[el-switch](https://element-plus.gitee.io/zh-CN/component/switch.html#attributes)

| 属性名           | 说明                          | 类型       | 默认值   |
   |---------------|-----------------------------|----------|-------|
| col           | 响应式宽度设置                                                                           | Number             | 24      |
| activeText    | switch 打开时的文字描述             | Stringt  | 是     |
| inactiveText  | switch 的状态为 off 时的文字描述      | String   | 否     |
| disabled      | 禁用                          | Boolean  | false |
| activeValue   | switch 状态为 on 时的值           | Boolean  | true  |
| inactiveValue | switch的状态为 off 时的值          | Boolean  | false |
| inlinePrompt  | 无论图标或文本是否显示在点内，只会呈现文本的第一个字符 | Boolean  | false |
| change        | 	选中值发生变化时触发                 | Function | 	-    |
##### 6. checkbox 多选框

```
// 基础
{label: '活动类型', key: 'type', type:'checkbox', children:[ {label:'在线活动'},{label:'促销活动'},{label:'线下活动'}] },


```
配置维度是依赖[el-switch](https://element-plus.gitee.io/zh-CN/component/switch.html#attributes)

| 属性名              | 说明         | 类型       | 默认值   |
|------------------|------------|----------|-------|
| col           | 响应式宽度设置                                                                           | Number             | 24      |
| props.trueLabel  | 选中时的值      | Stringt  | -     |
| props.falseLabel | 没有选中时的值    | String   | -     |
| disabled         | 禁用         | Boolean  | false |
| props.trueValue  | 选中时的值      | Boolean  | -     |
| props.falseValue | 没有选中时的值    | Boolean  | -     |
| change           | 选中值发生变化时触发 | Function | 	-    |
##### 7. radio 单选框

```
// 基础
{ key: 'openStatus', label: '模板状态', type: 'radio', children: [{ label: '启用', value: 1 }, { label: '禁用', value: 0 },] }

```
配置维度是依赖[el-radio](https://element-plus.gitee.io/zh-CN/component/radio.html#radio-attributes)

| 属性名         | 说明            | 类型       | 默认值   |
|-------------|---------------|----------|-------|
| col           | 响应式宽度设置                                                                           | Number             | 24      |
| props.value | 单选框的值         | Stringt  | -     |
| props.label | 单选框的 label    | String   | -     |
| disabled    | 禁用            | Boolean  | false |
| width       | 所有单个 radio的宽度 | Number   | -     |
| change      | 选中值发生变化时触发    | Function | 	-    |
##### 8. cascader 级联选择器

```
// 基础
{key: 'dataPermissionCode', label: '所属区域', type: 'cascader', children: [{ label: '启用', value: 1 }, { label: '禁用', value: 0 },]}

// 接口请求
{key: 'dataPermissionCode', label: '所属区域', col: 12, placeholder: '请选择所属区域', remote: true, type: 'cascader', url: '/system/area/getAreaDtoListByParentCode/', method: 'get', params: { default: 0 },paramsType: 'url', props: { label: 'name', value: 'code' },}


```
配置维度是依赖[el-cascader](https://element-plus.gitee.io/zh-CN/component/cascader.html#cascader-attributes)

| 属性名          | 说明                                                                                       | 类型       | 默认值   |
|--------------|------------------------------------------------------------------------------------------|----------|-------|
| col           | 响应式宽度设置                                                                           | Number             | 24      |
| props        | 详情配置请看[props](https://element-plus.gitee.io/zh-CN/component/cascader.html#cascaderprops) | Object   | -     |
| options      | 选项的数据源， value 和 label 可以通过 CascaderProps 自定义.                                            | Object   | []    |
| disabled     | 禁用                                                                                       | Boolean  | false |
| collapseTags | 多选模式下是否折叠Tag                                                                             | Boolean  | -     |
| change       | 选中值发生变化时触发                                                                               | Function | 	-    |

[//]: # (##### 9. esDate)

[//]: # (##### 10. allSelect)


### 弹框组件（TwDialog）

> 基于[ElDialog](https://element-plus.gitee.io/zh-CN/component/dialog.html#attributes)二次封装。内置了全屏和默认高度，底部按钮等操作。可配合自定义form动态开发

```
// 基础（默认不显示底部操作开关）
<tw-dialog title="消息列表" v-model="dialogFlag">
   内容
</tw-dialog>

// 接口请求 （显示底部操作按钮）
<tw-dialog title="消息列表" v-model="dialogFlag" isBtns>
   内容
</tw-dialog>

```
#### TwDialog 属性
| 属性名               | 说明                                                     | 类型            | 默认值    |
|-------------------|--------------------------------------------------------|---------------|--------|
| title             | dialog弹框title配置                                        | String        | 新增     |
| modelValue        | dialog弹框显示隐藏变量                                         | Boolean       | false  |
| cancelBtn         | 取消按钮自定义文字配置                                            | String        | 取消     |
| confirmBtn        | 确定按钮自定义文字配置                                            | String        | 	确定    |
| cancelLoading     | 取消按钮loading配置                                          | Boolean       | 	false |
| loading           | 确定按钮loading配置                                          | Boolean       | 	false |
| width             | 自定义宽度                                                  | Number/String | 	80%   |
| height            | 自定义高度                                                  | Number/String | 	100%  |
| btns              | 底部自定义操作按钮 { type:'默认： primary',key:'事件名称',name:'按钮名称'} | Array         | 	[]    |
| isBtns            | 底部操作按钮显示开关                                             | Boolean       | 	false |
| bodyPadding       | 内容内填充                                                  | Number        | 	20    |
| closeOnClickModal | 是否可以通过点击modal关闭Dialog                                  | Boolean       | 	true  |
| isFull            | 是否显示全屏开关                                               | Boolean       | 	true  |
| appendToBody      | 是否插入body                                               | Boolean       | 	false |
| dialogTop         | 顶部距离-特殊场景使用                                            | Number        | 	100   |
| leftHeight        | 剩余距离-特殊场景使用                                            | leftHeight    | 	140   |

#### TwDialog 事件
| 事件名          | 说明     | 类型       |
|--------------|--------|----------|
| onScreenfull | 全屏事件   | Function |
| save         | 确定事件   | Function |
| cancel       | 取消事件   | Function |
| handleClose  | 关闭弹框事件 | Function |



### 抽屉组件（TwDrawer）

> 基于[ElDrawer](https://element-plus.gitee.io/zh-CN/component/drawer.html#%E5%B1%9E%E6%80%A7)二次封装。内置了全屏和默认高度，底部按钮等操作。可配合自定义form动态开发

```
// 基础（默认显示底部操作开关）且默认高度全屏
<tw-drawer :loading="loading" :title="title" v-model="drawerFlag">
   内容
</tw-drawer>

// 接口请求 （不显示底部操作按钮）
<tw-drawer @handleClose="handleClose" :loading="loading" :title="title" :isBtns="false" v-model="drawerFlag">
   内容
</tw-drawer>

```
#### TwDrawer 属性
| 属性名               | 说明                                                     | 类型            | 默认值    |
|-------------------|--------------------------------------------------------|---------------|--------|
| title             | dialog弹框title配置                                        | String        | 新增     |
| modelValue        | dialog弹框显示隐藏变量                                         | Boolean       | false  |
| cancelBtn         | 取消按钮自定义文字配置                                            | String        | 取消     |
| confirmBtn        | 确定按钮自定义文字配置                                            | String        | 	确定    |
| cancelLoading     | 取消按钮loading配置                                          | Boolean       | 	false |
| loading           | 确定按钮loading配置                                          | Boolean       | 	false |
| width             | 自定义宽度                                                  | Number/String | 	800px |
| top               | 自定义高度                                                  | String        | 	60px  |
| btns              | 底部自定义操作按钮 { type:'默认： primary',key:'事件名称',name:'按钮名称'} | Array         | 	[]    |
| isBtns            | 底部操作按钮显示开关                                             | Boolean       | 	true  |
| isScreenfull      | 内容内填充                                                  | Boolean       | 	true  |
| defaultScreenfull | 是否可以通过点击modal关闭Dialog                                  | Boolean       | 	false |

#### TwDrawer 事件
| 事件名          | 说明       | 类型       |
|--------------|----------|----------|
| onScreenfull | 全屏事件     | Function |
| save         | 确定事件     | Function |
| handleClose  | 取消事件     | Function |
| beforeClose  | 关闭抽屉前的事件 | Function |
| cancel       | 关闭弹框事件   | Function |

### 描述组件（TwFormItem）

> 基于[TwForm](https://element-plus.gitee.io/zh-CN/component/drawer.html#%E5%B1%9E%E6%80%A7)render函数二次封装。核心用于动态显示

```
<template>
   <tw-form-item label-width="86px" size="small" :lists="mainData.aduitResultList" :row="mainData.formData"></tw-form-item>
</template>

<script setup lang="tsx">
let mainData = reactive({
    aduitResultList:[
        { key: 'plateNum', label: '车牌号：', col: 8 ,labelWidth:'76px'},
        { key: 'plateColorName', label: '车牌颜色：', col: 8 },
        { key: 'alarmTypeName', label: '报警类型：', col: 8 },
        { key: 'eventStartTime', label: '报警开始时间：' , col: 8,labelWidth:'110px'},
        { key: 'eventEndTime', label: '报警结束时间：' , col: 8,labelWidth:'110px'},
        { key: 'eventPersistSecondsStr', label: '持续时长：' , col: 8},
        { key: 'companyName', label: '企业信息：' , col: 8},
    ]
    formData: {
        plateNum: 1,
        plateColorName: 25,
        "alarmTypeName": "3",
        "eventStartTime": "4",
        "eventEndTime": "5",
        "eventPersistSecondsStr": "6",
        "companyName": "7",
    }
})

</script>

```
#### TwFormItem 属性
| 属性名        | 说明        | 类型     | 默认值   |
|------------|-----------|--------|-------|
| lists      | 渲染数据配置    | Array  | []    |
| labelWidth | 默认label宽度 | String | 100px |
| size       | 默认显示格式    | String | -     |
| mb         | 底部外边距     | String | 	4px  |

##### lists 属性
| 属性名        | 说明                                                                                          | 类型       | 默认值    |
|------------|---------------------------------------------------------------------------------------------|----------|--------|
| col        | 列宽比例依赖[Layout 布局](https://element-plus.gitee.io/zh-CN/component/layout.html#row-attributes) | Number   | 12     |
| label      | label的描述字段                                                                                  | String   | -      |
| labelWidth | 自定义label宽度                                                                                  | String   | 100px  |
| slot       | 自定义显示内容状态                                                                                   | Boolean  | 	false |
| key        | 显示对应的key                                                                                    | String   | 	-     |
| formatter  | 自定义显示内容                                                                                     | Function | 	-     |


### 分页组件（TwPagination）
> 基于[ElPagination](https://element-plus.gitee.io/zh-CN/component/pagination.html#%E5%B1%9E%E6%80%A7)二次封装，内置只能中文。注意：此组件并没有集成所有配置，请适应场景使用


#### TwFormItem 属性
| 属性名          | 说明             | 类型      | 默认值                                      |
|--------------|----------------|---------|------------------------------------------|
| currentPage  | 当前页            | Number  | 1                                        |
| pageSize     | 每页显示条数         | Number  | 25                                       |
| total        | 总页数            | Number  | 0                                        |
| size         | table 大小配置     | String  | 	default                                 |
| disabled     | 每页显示条数         | Boolean | 	false                                   |
| pageSizesArr | 每页显示个数选择器的选项设置 | Array   | 	[25, 50, 100]                           |
| layout       | 组件布局，子组件名用逗号分隔 | String  | 	total, prev, pager, next, sizes, jumper |
| background   | 是否为分页按钮添加背景色   | Boolean | 	true                                    |

### 表格（TwTable）[TwTable]
> 基于[TwTable](https://element-plus.gitee.io/zh-CN/component/table.html#table-%E5%B1%9E%E6%80%A7)二次封装且是和分页组件合并使用。内置自动计算高度，api请求，自定义data数据等功能。ContentMain组件也是当前组件组合使用。组件内部没有集成表格树（因为大批量行列数据会容易照成卡顿）。请适应场景使用

```
<template>
    // 基础渲染
   <TwTable :columns="columns" :lists="mainData.dataLists"></TwTable>
   
   // api渲染
   <TwTable
        ref="TwTable"
        id="regulators_acopy_releasedetails_table_view"
        url="/business/noticePublishScopeEntity/page"
        method="post"
        :tableParams="{ noticeId: rowData.id }"
        :botHeight="100"
        :columns="mainData.columns"
    ></TwTable>
</template>

<script setup lang="tsx">
let TwTable = $ref(null)

let mainData = reactive({
    // tableUrl: '/business/noticePublishScopeEntity/page',
    formData: {
        noticeId: 0,
        order: '',
        orderField: '',
        pageNumber: 1,
        pageSize:25,
    },
    columns: [
        { prop: 'businessName', label: '发布对象' },
        { prop: 'createTime', label: '创建时间' },
        { prop: 'lastModifyTime', label: '发布时间' },
    ],
    dataLists:[ {businessName:'1', createTime:'2', lastModifyTime:'3',}]
})

// 重新请求表格数据（url场景使用）
const getTable = () => {
    TwTable.getTable()
}
</script>

```

#### TwTable 属性
| 属性名                 | 说明                                                                                         | 类型       | 默认值                                     |
|---------------------|--------------------------------------------------------------------------------------------|----------|-----------------------------------------|
| url                 | 表格请求地址                                                                                     | String   | -                                       |
| id                  | table的唯一id，一页面多表格场景须赋值，否则无法动态计算高度                                                          | String   | -                                       |
| method              | 表格动态url请求类型                                                                                | String   | post                                    |
| columns             | 表格每一列渲染数据                                                                                  | Array    | []                                      |
| lists               | 表格每一行渲染数据                                                                                  | Array    | []                                      |
| tableParams         | 表格默认请求数据，这里代指mainData.formData。他们是绑定关系                                                     | Object   | { pageNumber: 1, pageSize: 25 }         |
| tableHeight         | 表格固定高度                                                                                     | Number   | 0                                       |
| isRradio            | 单选                                                                                         | Boolean  | false                                   |
| showPagination      | 分页显示开关                                                                                     | Boolean  | true                                    |
| botHeight           | 是分页栏加上边距加底部空白位置高度                                                                          | Number   | 50                                      |
| indexFixed          | 索引左侧定位开关                                                                                   | Boolean  | false                                   |
| isCheck             | checkbox显示开关                                                                               | Boolean  | false                                   |
| checkFixed          | checkbox左侧定位开关                                                                             | String   | 'left'                                  |
| showIndex           | 索引显示开关                                                                                     | Boolean  | false                                   |
| size                | 表格大小配置                                                                                     | String   | 'large'                                 |
| stripe              | 斑马纹显示开关                                                                                    | Boolean  | true                                    |
| border              | 边框线显示开关                                                                                    | Boolean  | false                                   |
| pageSizesArr        | 分页页码                                                                                       | Array    | [25, 50, 100]                           |
| tableBtns           | 单例按钮渲染数据                                                                                   | Object   | -                                       |
| small               | 每页显示条数                                                                                     | Boolean  | false                                   |
| headBtns            | 顶部操作按钮                                                                                     | Array    | []                                      |
| isTableHeader       | 默认显示表格顶部操作栏                                                                                | Boolean  | true                                    |
| isLoadStatus        | 默认首次不加载table数据                                                                             | Boolean  | false                                   |
| highlightCurrentRow | 是否高亮显示当前选中行                                                                                | Boolean  | true                                    |
| rowClassName        | 行样式名称，同[elTable](https://element-plus.gitee.io/zh-CN/component/table.html)一样               | Function | -                                       |
| selectable          | isCheck必须为true 用于判断选项是否可以点击，返回true或false                                                   | Function | { return true}                          |
| autoScrollToTop     | 请求数据后是否自动滚动到顶部                                                                             | Boolean  | true                                    |
| layout              | 表格分页布局默认配置，依赖[elPagination](https://element-plus.gitee.io/zh-CN/component/pagination.html) | String   | total, prev, pager, next, sizes, jumper |
| background          | 表格的背景颜色                                                                                    | Boolean  | true                                    |
| loadingBg           | 表格接口加载状态下背景颜色                                                                              | String   | rabg(225,225,225)                       |
| emptyText           | 列表无数据时展示的文字                                                                                | String   | 暂无数据                                    |

#### TwTable 事件
| 事件名                | 说明                                                                                                     | 类型       |
|--------------------|--------------------------------------------------------------------------------------------------------|----------|
| tableCallback      | 表格api加载完成后响应的事件                                                                                        | Function |
| checkSelect        | 表格点击checkbox响应的事件，对应[elTable](https://element-plus.gitee.io/zh-CN/component/table.html) - select事件     | Function |
| rowCheck           | 表格点击行数据响应的事件                                                                                           | Function |
| tableCurrentChange | 当表格的当前行发生变化的时候会触发该事件，如果要高亮当前行，请打开表格的 highlight-current-row 属性                                          | Function |
| selectionChange    | 表格的check选择， 对应[elTable](https://element-plus.gitee.io/zh-CN/component/table.html) - selection-change事件 | Function |
