import {resolve} from 'path'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import Icons from 'unplugin-icons/vite'
import IconsResolver from 'unplugin-icons/resolver'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
import ReactivityTransform from '@vue-macros/reactivity-transform/vite'
import vueSetupExtend from 'vite-plugin-vue-setup-extend'

const resolveSrc = resolve(__dirname, 'src')

export const proConfig =  (mode:any)=> {
    return {
        // build 独有配置
        server:{
            hmr:{
                overlay: true    // 默认是 true，设置为false后报错不会在以vite的形式在页面显示了
            }
        },
        define: {
            'process.env': {
                VUE_APP_BASE_API: ""
            }
        },
        base: './',
        resolve: {
            alias: {
                '@': resolveSrc,
            },
            dedupe: [
                'vue'
            ]
        },
        css: {
            preprocessorOptions: {
                scss: {
                    // additionalData: `@use "@/styles/element/index.scss" as *;`,
                },
            },
        },
        esbuild: {
            jsxFactory: 'h',
            jsxFragment: 'Fragment',

            // drop: command === 'build' ? ['console', 'debugger'] : []
        },
        chainWebpack: (config:any) => {
            // `删除需要预先加载(当前页面)的资源，当需要这些资源的时候，页面会自动加载`
            config.plugins.delete('preload')
            // `删除需要预先获取(将来的页面)的资源`
            config.plugins.delete('prefetch')
        },
        build: {
            lib: {
                entry: resolve(__dirname, "src/index.js"),
                name: "tw-vue-el",
                fileName: (format:any) => `tw-vue-el.${format}.js`,
            },
            rollupOptions: {
                // 确保外部化处理那些你不想打包进库的依赖
                external: ["vue"],
                output: {
                    // 在 UMD 构建模式下为这些外部化的依赖提供一个全局变量
                    globals: {
                        vue: "Vue",
                    },
                },
            },
            sourcemap: false, // 输出.map文件,默认是false
            brotliSize: false, // 设置为false将禁用构建的brotli压缩大小报告。可以稍微提高构建速度
            // terserOptions: {
            //     compress: {
            //         //生产环境时移除console
            //         drop_console: true,
            //         drop_debugger: true,
            //     },
            // }
        },
        plugins: [
            vue(),
            vueSetupExtend(),
            ReactivityTransform(),
            vueJsx({
                // options are passed on to @vue/babel-plugin-jsx
            }),
            AutoImport({
                imports: ['vue', 'vue-router', '@vueuse/core', 'pinia'],
                dts: 'src/atuoFlies/auto-imports.d.ts',
                resolvers: [
                    // 自动导入图标组件
                    IconsResolver({
                        prefix: 'Icon',
                    }),
                    ElementPlusResolver()
                ],
            }),
            Components({
                dts: 'src/atuoFlies/components.d.ts',
                resolvers: [
                    // 自动注册图标组件
                    IconsResolver({
                        enabledCollections: ['ep'],
                    }),
                    // 自动导入 Element Plus 组件
                    ElementPlusResolver(),
                ],
            }),
            Icons({
                autoInstall: true,
            }),
        ],
    }
}

